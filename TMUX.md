# TMUX cheat sheet

## Sessions

### In opened session press
ctrl+j d  (detach)

### Attach session
$ tmux attach -t session_name

### Switch between sessions
ctrl+j s

