# CUSTOM CONFIGURATION FILES FOR VIM


https://github.com/szw/vim-tags

## Ubuntu
sudo apt-get install exuberant-ctags

## Mac
brew install ctags

## Install Vundle at first
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

## To resolv "Sorry, no help for help.txt"
:helptags $VIMRUNTIME/doc

## To generate ctags
### Into project run this command
ctags -R .

# Add this to .bashrc on linux
vi ~/.bashrc
alias tmux="TERM=screen-256color-bce tmux"
source ~/.bashrc

# Install Silver search
brew install the_silver_searcher

apt-get install silversearcher-ag

yum install the_silver_searcher


# SETUP
ln -s ~/vim-kenji/vimrc ~/.vimrc
ln -s ~/vim-kenji/vim ~/.vim
ln -s ~/vim-kenji/tmux.conf ~/.tmux.conf
